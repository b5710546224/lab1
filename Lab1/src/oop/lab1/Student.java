package oop.lab1;

/**
 * A simple model for a Student that extend from Person class with an id.
 * @author Natcha Pongsupanee 5710546224
 */
public class Student extends Person {

	private long id;
	
	/**
	 * Initialize a new Student.
	 * @param id is the ID of the Student
	 * @param name is the name of the Student
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	public boolean equals(Object obj) {
		if(obj == null){
			return false;
		}
		if(obj.getClass() != this.getClass()){
			return false;
		}
		Student other = (Student) obj;
		if(id == other.id){
			return true;
		}
		return false;
	}
}
